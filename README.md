# reputation

Library for analysing player reputation around the gaming world.

## Data sources

* [fishbans](http://fishbans.com/) via
  [fishbans](https://wtfork.org/nerdrage/fishbans) go library

## Installation

`go get wtfork.org/nerdrage/reputation`
